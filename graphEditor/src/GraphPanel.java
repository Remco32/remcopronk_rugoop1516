import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Used to draw vertices and edges
 * Created by Remco on 27-5-2016.
 */

public class GraphPanel extends JPanel implements Observer {
    /**
     * variables
     **/
    private GraphModel currentModel;

    /**
     * constructors
     **/

    //gets GraphModel as argument for it to keep using the same model
    public GraphPanel(GraphModel model, GraphFrame frame) {
        //Can't use the arguments of the constructor outside the constructor otherwise
        currentModel = model;

        //become observer of model
        model.addObserver(this);

        //Make the panel active in the frame to actually draw things
        frame.setContentPane(this);

    }

    /**
     * setters and getters
     **/

    //required for assignment
    public void setModel(GraphModel inputModel) {
        currentModel = inputModel;
    }

    //required for assignment
    public GraphModel getModel() {
        return currentModel;
    }

    /**
     * methods
     **/

    //Gets executed when it gets notified
    @Override
    public void update(Observable o, Object arg) {
        //System.out.println("Observer notified!");
        //redraw the panel
        repaint();
    }

    public void drawAllVertices(Graphics g) {
        for (GraphVertex current : currentModel.getVertexList()) {

            //print current vertex as a rectangle
            if (!current.getSelectedStatus()) {
                g.setColor(Color.white);
                g.fillRect(current.getVertexX(), current.getVertexY(), current.getVertexWidth(), current.getVertexHeight());
            }

            if (current.getSelectedStatus()) {
                g.setColor(Color.blue);
                g.fillRect(current.getVertexX(), current.getVertexY(), current.getVertexWidth(), current.getVertexHeight());
            }

            g.setColor(Color.black);
            //Draw name
            g.drawString(current.getVertexName(), (current.getVertexX() + 10), (current.getVertexY() + 20));

            //System.out.println("Vertex printed");
        }
    }

    public void drawAllEdges(Graphics g) {
        for (GraphEdge current : currentModel.getEdgeList()) {

            g.setColor(Color.black);

            GraphVertex vertex1 = current.getConnectedVertex1();
            GraphVertex vertex2 = current.getConnectedVertex2();

            //draw a line between the centers of both vertices
            g.drawLine(vertex1.getVertexX() + (vertex1.getVertexWidth() / 2),
                    vertex1.getVertexY() + (vertex1.getVertexHeight() / 2),
                    vertex2.getVertexX() + (vertex2.getVertexWidth() / 2),
                    vertex2.getVertexY() + (vertex2.getVertexHeight() / 2));

        }
    }

    //highlight a vertex
    public void highlightVertex(GraphVertex vertex) {
        //set previous values to correct ones
        for (GraphVertex x : currentModel.getVertexList()) {

            if (x.getSelectedStatus()) {
                x.setSelectedStatus(false);
            }
            //if(x.getSelectedStatus() == 2){
            //    x.setSelectedStatus(1);
            //}
        }
        vertex.setSelectedStatus(true);
        repaint();
    }

    public void callRepaint() {
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawAllEdges(g);
        drawAllVertices(g);
    }

}
