import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by Remco on 18-5-2016.
 */
public class GraphEdit {
    public static void main(String[] args) {
        //create a new GraphModel (our 'world')
        GraphModel model = new GraphModel();

        //take inputargument if it exists
        if (0 < args.length) {
            String filename = args[0];
            File file = new File(filename);
            //overwrite model from file
            try {
                model.loadFromFile(file);
            } catch (FileNotFoundException e) {
                System.err.println("File not found! " + e.getMessage());
                e.printStackTrace();
            }
        }

        // create a frame (window)
        GraphFrame frame = new GraphFrame(model);
        frame.setVisible(true); // make frame visible
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //kill process when the window is closed

        //create a panel (used to draw vertices etc)
        GraphPanel panel = new GraphPanel(model, frame);
        panel.setVisible(true); // make frame visible

        //enables clicking on the panel
        Component mouseClick = new SelectionController(model, panel, frame);
        frame.addMouseListener((MouseListener) mouseClick);
        frame.addMouseMotionListener((MouseMotionListener) mouseClick);
    }
}
