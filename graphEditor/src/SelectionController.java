import java.awt.*;
import java.awt.event.*;

/**
 * Created by Remco on 2-6-2016.
 */
public class SelectionController extends Component implements MouseListener, MouseMotionListener {

    /**
     * vars
     **/

    private int mouseX = -1;
    private int mouseY = -1;
    private int previousMouseX = -2;
    private int previousMouseY = -2;

    //Frame takes size away from the panel, this compensates it.
    final int FRAMEBIASX = 8;
    final int FRAMEBIASY = 53;

    GraphModel currentModel;
    GraphVertex selectedVertex;
    GraphPanel currentPanel;
    GraphFrame currentFrame;

    /**
     * constructors
     **/

    public SelectionController(GraphModel model, GraphPanel panel, GraphFrame frame) {
        currentModel = model;
        currentPanel = panel;
        currentFrame = frame;

    }

    //check if there is a vertex at this location
    public void checkForVertex(int x, int y) {
        for (GraphVertex vertex : currentModel.getVertexList()) {
            int vertexX = vertex.getVertexX();
            int vertexY = vertex.getVertexY();
            int vertexWidth = vertex.getVertexWidth();
            int vertexHeight = vertex.getVertexHeight();

            if (x > (vertexX) &&
                    x <= (vertexX + vertexWidth) &&
                    y > (vertexY) &&
                    y <= (vertexY + vertexHeight)) {

                selectedVertex = vertex;
                currentPanel.highlightVertex(vertex);
            }

        }
    }

    //Return the vertex without selecting it if it is on this location
    public GraphVertex checkForVertexReturn(int x, int y) {
        for (GraphVertex vertex : currentModel.getVertexList()) {
            int vertexX = vertex.getVertexX();
            int vertexY = vertex.getVertexY();
            int vertexWidth = vertex.getVertexWidth();
            int vertexHeight = vertex.getVertexHeight();

            if (x > (vertexX) &&
                    x <= (vertexX + vertexWidth) &&
                    y > (vertexY) &&
                    y <= (vertexY + vertexHeight)) {

                return vertex;
            }
        }
        return null;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        mouseX = e.getX() - FRAMEBIASX;
        mouseY = e.getY() - FRAMEBIASY;
        //System.out.println("There was a click at " + mouseX + "," + mouseY);

        checkForVertex(mouseX, mouseY);

    }

    //mouse is being pressed
    @Override
    public void mousePressed(MouseEvent e) {
        previousMouseX = e.getX() - FRAMEBIASX;
        previousMouseY = e.getY() - FRAMEBIASY;
    }

    //mouse is no longer pressed
    @Override
    public void mouseReleased(MouseEvent e) {

        //Change selection status of vertex when nothing is selected
        if (selectedVertex != null) {
            //change status in object
            selectedVertex.setSelectedStatus(false);
            //change status here
            selectedVertex = null;
            //repaint
            currentPanel.callRepaint();
        }

        //No longer drawing lines, cleanup panel
        if (!currentFrame.getIsAddingEdge() && !currentFrame.getIsRemovingEdge()) {
            currentPanel.callRepaint();
        }

        //Draw line when we are to create a new edge
        if (currentFrame.getIsAddingEdge()) {
            //vertex1 becomes check on original coordinates mouse
            GraphVertex vertex1 = checkForVertexReturn(previousMouseX, previousMouseY);

            //vertex2 becomes check of current coordinates mouse
            GraphVertex vertex2 = checkForVertexReturn(mouseX, mouseY);

            //check if both are not null, and if we are not having the same vertex in both slots
            if (vertex1 != null && vertex2 != null && !vertex1.equals(vertex2)) {
                //create new edge
                currentModel.addToEdgeList(vertex1, vertex2);
                //stop adding edges
                currentFrame.setIsAddingEdge(false);
            }

            currentPanel.callRepaint();
        }

        //Draw line when we are to delete a new edge
        if (currentFrame.getIsRemovingEdge()) {
            //vertex1 becomes check on original coordinates mouse
            GraphVertex vertex1 = checkForVertexReturn(previousMouseX, previousMouseY);

            //vertex2 becomes check of current coordinates mouse
            GraphVertex vertex2 = checkForVertexReturn(mouseX, mouseY);

            //check if both are not null, and if we are not having the same vertex in both slots
            if (vertex1 != null && vertex2 != null && !vertex1.equals(vertex2)) {
                //check if there is an edge that is connected between these vertices
                //By not using a foreach loop, a ConcurrentModificationException is avoided
                for (int i = 0; i < currentModel.getEdgeList().size(); i++) {
                    GraphEdge currentEdge = currentModel.getEdgeList().get(i);
                    //If the edge has both vertices in it's slots
                    if ((currentEdge.getConnectedVertex1() == vertex1 || currentEdge.getConnectedVertex1() == vertex2) &&
                            (currentEdge.getConnectedVertex2() == vertex1 || currentEdge.getConnectedVertex2() == vertex2)) {
                        //remove the edge
                        currentModel.removeFromEdgelist(currentEdge);
                        //stop removing edges
                        currentFrame.setIsRemovingEdge(false);
                    }

                }

            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX() - FRAMEBIASX;
        mouseY = e.getY() - FRAMEBIASY;

        //suppress looking for a vertex when we are drawing a line
        if (!currentFrame.getIsAddingEdge() && !currentFrame.getIsRemovingEdge()) {
            checkForVertex(mouseX, mouseY);
        }

        if (selectedVertex != null) {
            selectedVertex.setX(mouseX - selectedVertex.getVertexWidth() / 2);
            selectedVertex.setY(mouseY - selectedVertex.getVertexHeight() / 2);
            currentPanel.callRepaint();
            currentModel.setUpdated();
        }

        //draw lines in cases we are adding or removing edges
        if (currentFrame.getIsAddingEdge() || currentFrame.getIsRemovingEdge()) {
            //clear previous lines
            currentPanel.callRepaint();
            /** DOESN'T WORK
             if(currentFrame.isAddingEdge){
             //make edge green in case of adding
             currentPanel.getGraphics().setColor(Color.green);
             }
             if(currentFrame.isRemovingEdge){
             //make edge red in case of removing
             currentPanel.getGraphics().setColor(Color.red);
             }
             **/
            //draw line on the panel
            currentPanel.getGraphics().drawLine(previousMouseX, previousMouseY, mouseX, mouseY);

        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
