/**
 * Created by Remco on 18-5-2016.
 */
public class GraphEdge {

    /**
     * variables
     **/
    //variables for both vertexes
    private GraphVertex connectedVertex1;
    private GraphVertex connectedVertex2;

    /**
     * constructors
     **/
    public GraphEdge(GraphVertex vertex1, GraphVertex vertex2) {
        connectedVertex1 = vertex1;
        connectedVertex2 = vertex2;
    }

    /**
     * setters and getters
     **/

    public GraphVertex getConnectedVertex1() {
        return connectedVertex1;
    }

    public GraphVertex getConnectedVertex2() {
        return connectedVertex2;
    }

    /** methods **/
}
