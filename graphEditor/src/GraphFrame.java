import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/**
 * Framework with buttons etc
 * Created by Remco on 27-5-2016.
 */
public class GraphFrame extends JFrame {

    private JFrame frame = new JFrame();
    private JMenuBar menuBar = new JMenuBar();

    private boolean isAddingEdge = false;
    private boolean isRemovingEdge = false;

    //gets GraphModel as argument for it to keep using the same model
    public GraphFrame(GraphModel model) {

        this.setSize(600, 400); // default size
        this.setTitle("Graph Editor by s2533081"); // set the title
        this.getContentPane().setLayout(new FlowLayout()); //define layout

        /** Actions for buttons using subclasses **/

        class SaveAction extends AbstractAction {
            public SaveAction(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            //specify what has to happen on press of the button
            public void actionPerformed(ActionEvent e) {
                //Create a file chooser
                final JFileChooser fc = new JFileChooser();

                //In response to a button click:
                int userSelection = fc.showSaveDialog(GraphFrame.this);

                //If the user selects save option...
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fc.getSelectedFile();
                    try {
                        model.saveToFile(fileToSave);
                    } catch (FileNotFoundException e1) {
                        System.err.println("Can't write to file!");
                        e1.printStackTrace();
                    }
                }
            }
        }

        class LoadAction extends AbstractAction {
            public LoadAction(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            //specify what has to happen on press of the button
            public void actionPerformed(ActionEvent e) {
                //create filechooser to load files
                final JFileChooser fc = new JFileChooser();

                if (fc.showOpenDialog(GraphFrame.this) == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    // load from file
                    try {
                        model.loadFromFile(file);
                    } catch (FileNotFoundException e1) {
                        System.err.println("File not found! " + e1.getMessage());
                        e1.printStackTrace();
                    }
                }
            }
        }

        class QuitAction extends AbstractAction {
            public QuitAction(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            //specify what has to happen on press of the button
            public void actionPerformed(ActionEvent e) {
                //terminate the program
                System.exit(0);
            }
        }

        class UndoAction extends AbstractAction {
            public UndoAction(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            public void actionPerformed(ActionEvent e) {
                //specify what has to happen on press of the button
                model.undo();
            }
        }

        class RedoAction extends AbstractAction {
            public RedoAction(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            public void actionPerformed(ActionEvent e) {
                //specify what has to happen on press of the button
                model.redo();
            }
        }

        /** Only used for debugging purposes
         *
         class NewVertexAction extends AbstractAction {
         public NewVertexAction(String text, Integer mnemonic) {
         super(text);
         putValue(MNEMONIC_KEY, mnemonic);
         }
         public void actionPerformed(ActionEvent e) {
         //specify what has to happen on press of the button
         GraphVertex vertex = new GraphVertex();
         model.addToVertexList(vertex);
         }
         }
         **/

        class NewVertexActionFull extends AbstractAction {
            public NewVertexActionFull(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            public void actionPerformed(ActionEvent e) {
                //specify what has to happen on press of the button

                //create dialog
                JTextField nameField = new JTextField(15);
                JTextField xField = new JTextField(5);
                JTextField yField = new JTextField(5);
                JTextField widthField = new JTextField(5);
                JTextField heightField = new JTextField(5);

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("name:"));
                myPanel.add(nameField);
                myPanel.add(new JLabel("x:"));
                myPanel.add(xField);
                myPanel.add(Box.createHorizontalStrut(15)); // a spacer
                myPanel.add(new JLabel("y:"));
                myPanel.add(yField);
                myPanel.add(new JLabel("width:"));
                myPanel.add(widthField);
                myPanel.add(new JLabel("height:"));
                myPanel.add(heightField);

                JOptionPane.showConfirmDialog(null, myPanel,
                        "Enter vertex information", JOptionPane.OK_CANCEL_OPTION);

                //Turn input into vertex
                try {
                    GraphVertex vertex = new GraphVertex(nameField.getText(), Integer.parseInt(xField.getText()), Integer.parseInt(yField.getText()), Integer.parseInt(widthField.getText()), Integer.parseInt(heightField.getText()));
                    model.addToVertexList(vertex);
                }
                //Catches both cases where non-numbers are used, and no input at all
                catch (NumberFormatException except) {
                    System.err.println("Not a number");
                    JOptionPane.showMessageDialog(frame,
                            "Please fill in numbers for the coordinates and location.",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        class NewEdgeAction extends AbstractAction {
            public NewEdgeAction(String text, Integer mnemonic) {
                super(text);
                putValue(MNEMONIC_KEY, mnemonic);
            }

            public void actionPerformed(ActionEvent e) {

                //if there are two or more vertices
                if (model.getVertexList().size() >= 2) {
                    //Start the ability of drawing lines to connect edges
                    isAddingEdge = true;
                } else {
                    JOptionPane.showMessageDialog(frame,
                            "Not enough vertices exist, at least two are needed.",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        class RemoveVertex extends AbstractAction {
            public RemoveVertex(String text, Integer mnemonic) {
                super(text);

                putValue(MNEMONIC_KEY, mnemonic);
            }

            //TODO only active when there's a vertex
            public void actionPerformed(ActionEvent e) {
                boolean vertexFound = false;
                //specify what has to happen on press of the button
                //if list is not empty
                if (!model.getVertexList().isEmpty()) {
                    //check for selected vertex
                    //By not using a foreach loop, a ConcurrentModificationException is avoided
                    for (int i = 0; i < model.getVertexList().size(); i++) {
                        GraphVertex vertex = model.getVertexList().get(i);

                        //ListIterator it = model.getVertexList().listIterator();
                        if (vertex.getSelectedStatus()) {
                            GraphModel.removeFromVertexList operation = new GraphModel().new removeFromVertexList(vertex, model);
                            //System.out.println("Size of "  + e.getMessage());
                            /** **/
                            //operation.removeFromVertexList(vertex, model);

                            vertexFound = true;
                        }
                    }
                    //Workaround for not deactivating menu option
                    if (!vertexFound) {
                        JOptionPane.showMessageDialog(frame,
                                "Please select a vertex first.",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }

        class RemoveEdge extends AbstractAction {
            public RemoveEdge(String text, Integer mnemonic) {
                super(text);

                putValue(MNEMONIC_KEY, mnemonic);
            }

            public void actionPerformed(ActionEvent e) {

                //if there are edges
                if (!model.getEdgeList().isEmpty()) {
                    //Start the ability of drawing lines to DELETE edges
                    isRemovingEdge = true;
                } else {
                    JOptionPane.showMessageDialog(frame,
                            "There are no edges to delete.",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        class ChangeVertexName extends AbstractAction {
            public ChangeVertexName(String text, Integer mnemonic) {
                super(text);

                putValue(MNEMONIC_KEY, mnemonic);
            }

            //TODO only active when there's a vertex
            public void actionPerformed(ActionEvent e) {

                boolean vertexFound = false;
                if (!model.getVertexList().isEmpty()) {
                    //check for selected vertex
                    for (GraphVertex vertex : model.getVertexList()) {
                        if (vertex.getSelectedStatus()) {

                            //create dialog
                            JTextField nameField = new JTextField(15);
                            JPanel myPanel = new JPanel();
                            myPanel.add(new JLabel("name:"));
                            myPanel.add(nameField);

                            JOptionPane.showConfirmDialog(null, myPanel,
                                    "Enter new name for " + vertex.getVertexName(), JOptionPane.OK_CANCEL_OPTION);

                            vertex.setVertexName(nameField.getText());
                            repaint();

                            vertexFound = true;
                        }

                    }
                    //Workaround for not deactivating menu option
                    if (!vertexFound) {
                        JOptionPane.showMessageDialog(frame,
                                "Please select a vertex first.",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }

        class CreateCopy extends AbstractAction {
            public CreateCopy(String text, Integer mnemonic) {
                super(text);

                putValue(MNEMONIC_KEY, mnemonic);
            }

            public void actionPerformed(ActionEvent e) {
                //create new frame with current model
                GraphFrame newFrame = new GraphFrame(model);
                newFrame.setVisible(true); // make frame visible

                //make a new panel in this new frame
                GraphPanel newPanel = new GraphPanel(model, newFrame);
                newPanel.setVisible(true); // make panel visible

                //TODO Make it possible to make changes in a cloned window. Panel doesn't function.
                //new selectioncontroller
                Component mouseClick = new SelectionController(model, newPanel, newFrame);
                frame.addMouseListener((MouseListener) mouseClick);
                frame.addMouseMotionListener((MouseMotionListener) mouseClick);

            }
        }

        /** menubar and its items **/
        this.setJMenuBar(menuBar); // create menuBar

        // FILE menu

        JMenu menuFile = new JMenu("FILE..."); // create new menu item
        menuBar.add(menuFile); // add menu item to bar

        Action saveAction = new SaveAction("Save...", KeyEvent.VK_L);
        JMenuItem menuItemSave = new JMenuItem(saveAction); // creates save button
        menuFile.add(menuItemSave); //adds save button to menuEdit

        Action loadAction = new LoadAction("Load...", KeyEvent.VK_L);
        JMenuItem menuItemLoad = new JMenuItem(loadAction);
        menuFile.add(menuItemLoad);

        menuFile.addSeparator(); // adds a line

        Action createCopyAction = new CreateCopy("Create a copy of the frame", KeyEvent.VK_L);
        JMenuItem menuCreateCopy = new JMenuItem(createCopyAction);
        menuFile.add(menuCreateCopy);

        menuFile.addSeparator(); // adds a line

        Action quitAction = new QuitAction("Quit", KeyEvent.VK_L);
        JMenuItem menuItemQuit = new JMenuItem(quitAction);
        menuFile.add(menuItemQuit);

        // EDIT menu

        JMenu menuEdit = new JMenu("EDIT..."); // create new menu item
        menuBar.add(menuEdit); // add menu item to bar

        Action undoAction = new UndoAction("Undo", KeyEvent.VK_L);
        JMenuItem menuItemUndo = new JMenuItem(undoAction);
        menuEdit.add(menuItemUndo);

        Action redoAction = new RedoAction("Redo", KeyEvent.VK_L);
        JMenuItem menuItemRedo = new JMenuItem(redoAction);
        menuEdit.add(menuItemRedo);

        menuEdit.addSeparator(); // adds a line

        /** Only used for debug purposes
         Action newVertexAction = new NewVertexAction("Add a default vertex", KeyEvent.VK_L);
         JMenuItem menuItemNewVertex = new JMenuItem(newVertexAction);
         menuEdit.add(menuItemNewVertex);
         **/

        Action NewVertexAction2 = new NewVertexActionFull("Add a new vertex", KeyEvent.VK_L);
        JMenuItem menuItemNewVertex2 = new JMenuItem(NewVertexAction2);
        menuEdit.add(menuItemNewVertex2);

        Action newEdgeAction = new NewEdgeAction("Add an edge by drawing a line between vertices", KeyEvent.VK_L);
        JMenuItem menuItemNewEdge = new JMenuItem(newEdgeAction);
        menuEdit.add(menuItemNewEdge);

        menuEdit.addSeparator(); // adds a line

        Action changeVertexName = new ChangeVertexName("Change the name of selected vertex", KeyEvent.VK_L);
        JMenuItem menuChangeVertexName = new JMenuItem(changeVertexName);
        menuEdit.add(menuChangeVertexName);

        menuEdit.addSeparator(); // adds a line

        Action removeVertexAction = new RemoveVertex("Remove the selected vertex", KeyEvent.VK_L);
        JMenuItem menuItemRemoveVertex = new JMenuItem(removeVertexAction);
        menuEdit.add(menuItemRemoveVertex);
        //disable it
        //menuItemRemoveVertex.setEnabled(false);

        Action RemoveEdgeAction = new RemoveEdge("Remove an edge by drawing a line between connected vertices", KeyEvent.VK_L);
        JMenuItem menuItemRemoveEdge = new JMenuItem(RemoveEdgeAction);
        menuEdit.add(menuItemRemoveEdge);
        //disable it
        //menuItemRemoveVertex.setEnabled(false);



    }

    /**
     * methods
     **/

    public boolean getIsAddingEdge() {
        return isAddingEdge;
    }

    public void setIsAddingEdge(boolean bool) {
        isAddingEdge = bool;
    }

    public boolean getIsRemovingEdge() {
        return isRemovingEdge;
    }

    public void setIsRemovingEdge(boolean bool) {
        isRemovingEdge = bool;
    }

}



