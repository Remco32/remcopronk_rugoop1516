import javax.swing.*;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

//TODO Start classes with a capital letter

/**
 * Created by Remco on 18-5-2016.
 */
public class GraphModel extends Observable {

    /**
     * variables
     **/
    //Vertex list
    private List<GraphVertex> vertexList = new ArrayList<GraphVertex>();
    //Edge list
    private List<GraphEdge> edgeList = new ArrayList<GraphEdge>();

    UndoManager undoManager = new UndoManager();

    /**
     * constructors
     **/
    public GraphModel() {

    }

    //unused, but required for assignment
    public GraphModel(File inputFile) {
        try {
            loadFromFile(inputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * setters and getters
     **/

    public List<GraphVertex> getVertexList() {
        return vertexList;
    }

    public List<GraphEdge> getEdgeList() {
        return edgeList;
    }

    /**
     * methods
     **/
    //add vertex to list
    public void addToVertexList(GraphVertex vertex) {

        vertexList.add(vertex);

        setChanged();
        notifyObservers();
    }

    //remove vertex from list
    public class removeFromVertexList extends AbstractUndoableEdit {

        public removeFromVertexList(GraphVertex vertex, GraphModel model) {
            for (int i = 0; i < model.getEdgeList().size(); i++) {
                //get edge at current index
                GraphEdge edge = model.getEdgeList().get(i);
                //if our vertex is connected to that edge, delete the edge
                //TODO found bug where when a vertex has >2 edges, one does not get removed
                if ((edge.getConnectedVertex1() == vertex) || (edge.getConnectedVertex2() == vertex)) {
                    model.removeFromEdgelist(edge);
                }
            }
            model.vertexList.remove(vertex);
            setChanged();
            notifyObservers();

            model.undoManager.addEdit(this);
        }

        public void undo() {
            try {
                super.undo();
                undoManager.undo();
                setChanged();
                notifyObservers();
            } catch (CannotUndoException cre) {
                System.err.println("Cannot undo");
                cre.printStackTrace();
            }
        }

        public void redo() {
            try {
                undoManager.redo();
            } catch (CannotRedoException cre) {
                System.err.println("Cannot redo");
                cre.printStackTrace();
            }
        }

    }

    //add edge to list
    public void addToEdgeList(GraphVertex vertex1, GraphVertex vertex2) {

        GraphEdge edge = new GraphEdge(vertex1, vertex2);
        edgeList.add(edge);

        setChanged();
        notifyObservers();

    }

    //remove edge from list
    public void removeFromEdgelist(GraphEdge edge) {
        edgeList.remove(edge);

        setChanged();
        notifyObservers();
    }

    //save to file
    public void saveToFile(File savefile) throws FileNotFoundException {

        //System.out.println("Saving...");

        //No vertices, and thus no edges either
        if (vertexList.size() == 0) {
            Frame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "There are no vertices to be saved.",
                    "Saving error",
                    JOptionPane.ERROR_MESSAGE);
        } else {
            //start writing to a file
            PrintWriter output = null;
            try {
                output = new PrintWriter(savefile + ".txt");
            } catch (FileNotFoundException e) {
                System.err.println("Can't write to file!");
                e.printStackTrace();
            }

            //count amount of vertices and edges and print them
            output.println(vertexList.size() + " " + edgeList.size());
            //print each vertex in format "x y height width name"
            for (GraphVertex x : vertexList) {
                output.println(x.getVertexX() + " " + x.getVertexY() + " " + x.getVertexWidth() + " " + x.getVertexHeight() + " " + x.getVertexName());
            }

            //print each edge in format "vertex1 vertex2"

            //get the index of the vertex that is connected to this edge
            for (GraphEdge currentEdge : edgeList) {
                GraphVertex vertex1 = currentEdge.getConnectedVertex1();
                GraphVertex vertex2 = currentEdge.getConnectedVertex2();

                //go through whole vertexlist
                for (int i = 0; i < this.getVertexList().size(); i++) {
                    //found vertex
                    if (vertex1.equals(getVertexList().get(i))) {
                        output.print(i + " ");
                    }
                }
                for (int i = 0; i < this.getVertexList().size(); i++) {

                    if (vertex2.equals(getVertexList().get(i))) {
                        output.print(i);
                        //start a new line
                        output.println();
                    }
                }

            }
            //stop writing to file
            output.close();
        }
    }

    public void loadFromFile(File inputFile) throws FileNotFoundException {

        try {
            //clear old vertices (and edges)

            //while the vertexlist is not empty...
            while (!getVertexList().isEmpty()) {
                //delete vertex at first position of the list

                removeFromVertexList operation = new removeFromVertexList(getVertexList().get(0), this);
                //operation.removeFromVertexList(getVertexList().get(0), this);

            }

            //while the edgelist is not empty...
            while (!getEdgeList().isEmpty()) {
                //delete vertex at first position of the list
                removeFromEdgelist(getEdgeList().get(0));
            }

            //open the file as stream
            InputStream fis = new FileInputStream(inputFile);
            //read the stream
            InputStreamReader isr = new InputStreamReader(fis);
            //read line for line
            BufferedReader br = new BufferedReader(isr);
            //get line
            String line = br.readLine();
            //Scanner to get values
            Scanner scanner = new Scanner(line);

            //count vertices and edges from top of file

            int amountOfVertices = scanner.nextInt();
            int amountOfEdges = scanner.nextInt();
            scanner.close();

            //go through all the lines with vertices
            for (int i = 0; i < amountOfVertices; i++) {
                line = br.readLine();
                Scanner scanner2 = new Scanner(line);
                //get values
                int x = scanner2.nextInt();
                int y = scanner2.nextInt();
                int height = scanner2.nextInt();
                int width = scanner2.nextInt();
                String name = scanner2.nextLine();
                //create new vertex
                addToVertexList(new GraphVertex(name, x, y, height, width));

                scanner2.close();
            }

            //go through all the lines with edges
            for (int i = 0; i < amountOfEdges; i++) {
                line = br.readLine();
                Scanner scanner2 = new Scanner(line);

                //get values
                int firstVertexNumber = scanner2.nextInt();
                int secondVertexNumber = scanner2.nextInt();

                //create new edge
                addToEdgeList(getVertexList().get(firstVertexNumber), getVertexList().get(secondVertexNumber));

                scanner2.close();
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found!" + e.getMessage());
        } catch (IOException e) {
            System.err.println("IOException!");
        }
        //In case of loading a file that isn't a correct model
        catch (InputMismatchException e) {
            System.err.println("The given file is not a saved model!");
            Frame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "This file is not a saved model.",
                    "Loading error",
                    JOptionPane.ERROR_MESSAGE);
        }
        setChanged();
        notifyObservers();
    }

    public void setUpdated() {
        setChanged();
        notifyObservers();
    }

    public void undo() {

        undoManager.undo();

        setChanged();
        notifyObservers();
    }

    public void redo() {

        undoManager.redo();

        setChanged();
        notifyObservers();
    }

}
