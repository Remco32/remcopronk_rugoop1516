/**
 * Created by Remco on 18-5-2016.
 */
public class GraphVertex {

    /**
     * variables
     **/
    //field for name of the vertex
    private String vertexName;
    private int ownX;
    private int ownY;
    private int ownHeight;
    private int ownWidth;
    private boolean isSelected = false;

    //default values for vertex
    private final String DEFAULTNAME = "New Vertex";
    private final int DEFAULTX = 20;
    private final int DEFAULTY = 20;
    private final int DEFAULTWIDTH = 200;
    private final int DEFAULTHEIGHT = 200;

    /** constructors **/

    /**
     * Only for debugging purposes
     * //constructs a default vertex
     * public GraphVertex(){
     * //default name
     * vertexName = DEFAULTNAME;
     * ownX = DEFAULTX;
     * ownY = DEFAULTY;
     * ownHeight = DEFAULTHEIGHT;
     * ownWidth = DEFAULTWIDTH;
     * }
     **/

    //constructs a vertex with custom name, location and size
    public GraphVertex(String name, int locationX, int locationY, int width, int height) {
        //store values in object
        vertexName = name;
        ownX = locationX;
        ownY = locationY;
        ownWidth = width;
        ownHeight = height;
    }

    /**
     * setters and getters
     **/
    public void setVertexName(String name) {
        this.vertexName = name;
    }

    public String getVertexName() {
        return vertexName;
    }

    //getters for location and size of vertex

    public int getVertexX() {
        return ownX;
    }

    public int getVertexY() {
        return ownY;
    }

    public int getVertexWidth() {
        return ownWidth;
    }

    public int getVertexHeight() {
        return ownHeight;
    }

    public boolean getSelectedStatus() {
        return isSelected;
    }

    public void setSelectedStatus(boolean bool) {
        isSelected = bool;
    }

    public void setX(int input) {
        ownX = input;
    }

    public void setY(int input) {
        ownY = input;
    }

}
